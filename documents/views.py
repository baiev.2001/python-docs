import datetime
import io

from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.contrib.auth.views import LoginView
from django.http import FileResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.generic import CreateView
from oauth2_provider.views import ProtectedResourceView
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm
from reportlab.pdfgen import canvas
from reportlab.pdfgen.canvas import Canvas

from .forms import UserForm, DocumentAddForm
from .services import *


class DocumentsLoginView(LoginView):
    template_name = 'documents/login-2.html'

    def get(self, request, *args, **kwargs):
        if isinstance(request.user, User):
            return redirect('/documents/user')
        else:
            return super().get(self, request, *args, **kwargs)


class DocumentsUserCreateView(CreateView):
    model = User
    form_class = UserForm
    template_name = 'documents/user_form.html'

    def get(self, request, *args, **kwargs):
        self.object = None
        if isinstance(request.user, User):
            return redirect('/documents/user')
        else:
            return super().get(self, request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form()
        if form.is_valid():
            form.instance.password = make_password(form.instance.password)
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        self.object = form.save()
        auth_login(self.request, self.object)
        return HttpResponseRedirect(self.get_success_url())


class DocumentAddView(CreateView, ProtectedResourceView):
    model = Document
    form_class = DocumentAddForm
    template_name = 'documents/document_upload.html'

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            form.instance.user_id = kwargs['userId']
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


def index(request):
    if isinstance(request.user, User):
        return redirect('/documents/user')
    return render(request, "documents/index.html")


@login_required()
def user_info(request):
    context = {'user': request.user}
    return render(request, 'documents/user-details.html', context)


@login_required()
def documents_list(request):
    documents_set = Document.objects.filter(user_id=request.user.id)
    context = {'documents': documents_set}
    return render(request, 'documents/documents-list.html', context)


@login_required()
def export_json(request):
    user_json = jsonify_user(request.user.id)
    document_json = jsonify_documents_by_user(request.user.id)
    buffer = io.BytesIO()
    buffer.write(bytes(user_json, 'utf-8'))
    buffer.write(bytes(document_json.__str__(), 'utf-8'))
    buffer.seek(0)
    return FileResponse(buffer, as_attachment=True)


@login_required()
def export_pdf(request):
    buffer = io.BytesIO()
    # Create the PDF object, using the buffer as its "file."
    p: Canvas = canvas.Canvas(buffer, pagesize=A4)
    # Draw things on the PDF. Here's where the PDF generation happens.
    # See the ReportLab documentation for the full list of functionality.
    text_obj = p.beginText()
    text_obj.setTextOrigin(A4[0] - 45 * mm, A4[1] - 5 * mm)
    text_obj.textLine(datetime.datetime.now().strftime("%Y:%m:%d - %H:%M:%S"))
    text_obj.moveCursor(-150 * mm, 5 * mm)
    user_to_render = User.objects.get(pk=request.user.id)
    documents_set = Document.objects.filter(user_id=request.user.id)
    text_obj.textLines(user_to_render.__str__())
    for doc in documents_set:
        text_obj.moveCursor(0, 10 * mm)
        text_obj.textLines(doc.__str__())

    # Close the PDF object cleanly, and we're done.
    p.drawText(text_obj)
    p.showPage()
    p.save()
    # FileResponse sets the Content-Disposition header so that browsers
    # present the option to save the file.
    buffer.seek(0)
    return FileResponse(buffer, as_attachment=True)


def login(request):
    return redirect(f'/o/authorize/?client_id={CLIENT_ID}&response_type=code&state=random_state_string')


def login_complete(request):
    response = requests.post('http://127.0.0.1:8000/o/token/',
                             data=f'code={request.GET.get("code")}&grant_type=authorization_code',
                             auth=HTTPBasicAuth(CLIENT_ID, CLIENT_SECRET),
                             headers={'CONTENT-TYPE': 'application/x-www-form-urlencoded; charset=UTF-8'})
    if response.status_code == 200:
        data = json.loads(response.text)
        request.session['token'] = data['access_token']
        return redirect('/documents/user')
    else:
        return redirect('/documents')


def logout(request):
    oauth_logout(request.session['token'])
    auth_logout(request)
    return redirect('/documents')
