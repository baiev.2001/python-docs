from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Fieldset
from django import forms

from documents.models import User, Document


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput, )

    class Meta:
        model = User
        fields = ('mail', 'name', 'password')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Save person'))


class DocumentAddForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ('file_name', 'content')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Upload'))
