"""labThree URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.auth.views import LoginView
from django.urls import path

from . import views

app_name = 'documents'

urlpatterns = [
    path('', views.index, name='documents_index'),
    path('login', views.login, name='login'),
    path('login/oauth', views.DocumentsLoginView.as_view(), name='documents_index'),
    path('login/complete', views.login_complete, name='documents_index'),
    path('logout', views.logout),
    path('user/', views.user_info, name='documents_user'),
    path('user/new', views.DocumentsUserCreateView.as_view(), name='user_form'),
    path('user/export/json', views.export_json, name='documents_user_doc_list'),
    path('user/export/pdf', views.export_pdf),
    path('user/document/list', views.documents_list, name='documents_user_doc_list'),
    path('user/document/new', views.DocumentAddView.as_view(), name='document_upload'),
]
