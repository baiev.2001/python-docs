import base64
import requests as requests
from django.contrib.auth import login as auth_login, logout as auth_logout

import json

from requests.auth import HTTPBasicAuth

from labThree.settings import CLIENT_ID, CLIENT_SECRET
from .models import *


def jsonify_user(id):
    user = User.objects.get(pk=id)
    return json.dumps(user, default=User.encode_user)


def jsonify_documents_by_user(id):
    documents = Document.objects.filter(user_id=id)
    json_docs = []
    for document in documents:
        json_docs.append(json.dumps(document, default=Document.encode_document))
    return json_docs


def oauth_logout(token):
    return requests.post('http://127.0.0.1:8000/o/revoke_token/',
                             data=f'token={token}&',
                             auth=HTTPBasicAuth(CLIENT_ID, CLIENT_SECRET),
                             headers={'CONTENT-TYPE': 'application/x-www-form-urlencoded; charset=UTF-8'})

