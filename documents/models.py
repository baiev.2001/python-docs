from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.files.storage import FileSystemStorage
from django.db import models

# Create your models here.
from documents.managers import DocumentsUserManager
from labThree.settings import BASE_DIR

path = BASE_DIR.__str__() + '/documents/files/content'

fs = FileSystemStorage(location=path)


class User(AbstractBaseUser, PermissionsMixin):
    mail = models.EmailField(max_length=200, unique=True)
    name = models.CharField(max_length=200)
    password = models.CharField(max_length=256)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    registration_date = models.DateTimeField('register date', auto_now=True)
    files = models.IntegerField(default=0)
    # token = models.CharField(max_length=256)
    EMAIL_FIELD = 'mail'
    USERNAME_FIELD = 'mail'
    REQUIRED_FIELDS = ('name', 'password')

    objects = DocumentsUserManager()

    def get_absolute_url(self):
        return "/documents/user"

    def __str__(self):
        return f"Mail: {self.mail}\n " \
               f"Name:  {self.name}\n " \
               f"Registered: {self.registration_date.strftime('%Y:%m:%d')}\n " \
               f"Files uploaded: {self.files} "

    def encode_user(obj):
        if isinstance(obj, User):
            return {
                "mail": obj.mail,
                "name": obj.name,
                "registered": obj.registration_date.strftime('%Y:%m:%d'),
                "files": obj.files
            }
        else:
            type_name = obj.__class__.__name__
            raise TypeError(f"Object of type '{type_name}' is not JSON serializable")


class Document(models.Model):
    file_name = models.CharField(max_length=200)
    download_date = models.DateTimeField('date downloaded', auto_now=True)
    size = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.FileField(null=True, storage=fs)

    def save(self, *args, **kwargs):
        file = self.content
        self.size = file.size
        if self.file_name is None or not self.file_name.strip():
            self.file_name = file.name
        super().save(*args, **kwargs)  # Call the "real" save() method.
        self.user.files += 1
        self.user.save()

    def get_absolute_url(self):
        return "/documents/user/document/list"

    def __str__(self):
        return f"File Name: {self.file_name}\n " \
               f"Size:  {self.size}\n " \
               f"Uploaded: {self.download_date.strftime('%Y:%m:%d - %H:%M:%S')}\n " \
               f"User: {self.user.name} "

    def encode_document(obj):
        if isinstance(obj, Document):
            return {
                "name": obj.file_name,
                "uploaded": obj.download_date.strftime('%Y:%m:%d - %H:%M:%S'),
                "size": obj.size,
                "user": obj.user.name
            }
        else:
            type_name = obj.__class__.__name__
            raise TypeError(f"Object of type '{type_name}' is not JSON serializable")
